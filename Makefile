CC      = gcc
CFLAGS  = -ggdb -W -O3
LDFLAGS = 
RM      = rm -f
PREFIX  = /usr/local

target    = tewneln
objects   = tewneln.o client.o
libraries = -lpthread

all: $(target)

$(target): $(objects)
	$(CC) -o $@ $(objects) $(LDFLAGS) $(libraries)

%.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS) || exit 1

clean:
	$(RM) $(objects) $(target)

install: $(target)
	install -m755 $(target) $(PREFIX)/bin

tewneln.c: tewneln.h client.h
client.c:  tewneln.h client.h

