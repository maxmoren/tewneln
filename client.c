#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#ifdef WIN32
#include <winsock.h>
#else
#include <signal.h>
#include <netdb.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif
#include <pthread.h>

#include "tewneln.h"
#include "client.h"

static void *client_main(void *arg)
{
	struct client *c = arg;

	char     buffer[BUFFER_SIZE];
	int      quit = FALSE;
	int      dl   = 0,
	         ul   = 0;
	int      rs, ws;

	int      ready;
	struct   timeval timeout;
	fd_set   fds;
	sigset_t mask;

	sigfillset(&mask);
	pthread_sigmask(SIG_BLOCK, &mask, NULL);

	while(!quit)
	{
		FD_ZERO(&fds);
		FD_SET(c->rsock, &fds);
		FD_SET(c->lsock, &fds);

		pthread_mutex_lock(&c->mutex);
		quit   = c->quit;
		c->ul += ul;
		c->dl += dl;
		pthread_mutex_unlock(&c->mutex);

		ul = 0;
		dl = 0;

		timeout.tv_sec  = 1;
		timeout.tv_usec = 0;

		ready = select(MAX(c->rsock, c->lsock) + 1, &fds, NULL, NULL, &timeout);

		if(ready < 0)
		{
			perror("select");
			break;
		}
		else if(ready == 0)
		{
			if(quit)
				break;
		}
		else
		{
			rs = ws = 0;

			if(FD_ISSET(c->rsock, &fds))
			{
			    rs = c->rsock;
				ws = c->lsock;
			}
			else if(FD_ISSET(c->lsock, &fds))
			{
			    rs = c->lsock;
				ws = c->rsock;
			}

			if(rs && ws)
				if(sendfile(ws, rs, NULL, BUFFER_SIZE) <= 0)
				{
					if(dl < 0 || ul < 0)
						perror("recv/send");

					pthread_mutex_lock(&c->mutex);
					c->quit = TRUE;
					pthread_mutex_unlock(&c->mutex);
					break;
				}
		}
	}

	puts("client disconnected");
	close(c->lsock);
	close(c->rsock);

	return NULL;
}

struct client *client_new(int lsock, int rsock)
{
	struct client *c;

	if((c = malloc(sizeof(struct client))) == NULL)
	{
		perror("malloc");
		return NULL;
	}

	c->quit  = FALSE;
	c->dl    = 0;
	c->ul    = 0;
	c->lsock = lsock;
	c->rsock = rsock;

	pthread_mutex_init(&c->mutex, NULL);

	if(pthread_create(&c->thread, NULL, client_main, c))
	{
		free(c);

		perror("pthread_create");
		return NULL;
	}

	return c;
}

int client_clean(struct client *c)
{
	int quit;

	pthread_mutex_lock(&c->mutex);
	quit = c->quit;
	pthread_mutex_unlock(&c->mutex);

	if(quit)
	{
		pthread_mutex_destroy(&c->mutex);
		free(c);

		return 1;
	}

	return 0;
}

void client_destroy(struct client *c)
{
	pthread_mutex_lock(&c->mutex);
	c->quit = TRUE;
	pthread_mutex_unlock(&c->mutex);

	pthread_join(c->thread, NULL);
	close(c->lsock);
	close(c->rsock);

	pthread_mutex_destroy(&c->mutex);
	free(c);
}

