#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#ifdef WIN32
#include <winsock.h>
#else
#include <netdb.h>
#include <sys/select.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

#include "tewneln.h"
#include "client.h"

static int sock = -1;
static struct client *clients[MAX_CLIENTS];

static void finish(void)
{
	size_t i;

#ifdef WIN32
	WSACleanup();
#endif

	if(sock >= 0)
		close(sock);

	for(i = 0; i < MAX_CLIENTS; i++)
		if(clients[i])
			client_destroy(clients[i]);

	puts("clean exit");
	exit(0);
}

static void handle_signal(int signal)
{
	printf("caught signal %d, exiting...\n", signal);
	finish();
}

static int parse_host(const char *str, char **dest_host, int *dest_port)
{
	int i, len = strlen(str);

	*dest_host = NULL;
	*dest_port = 0;

	for(i = 0; i < len; i++)
	{
		if(*(str + i) == ':')
		{
			*dest_host = malloc(i);
			*dest_port = atoi(str + i + 1);

			strncpy(*dest_host, str, i);

			return 0;
		}
	}

	return 1;
}

static int resolve(const char *host, struct sockaddr_in *addr)
{
	struct hostent *hostent;

	if((hostent = gethostbyname(host)) == NULL)
	{
		fprintf(stderr, "failed to resolve '%s'\n", host);
		return 1;
	}

	memcpy(&addr->sin_addr, hostent->h_addr, hostent->h_length);

	return 0;
}

static size_t human_readable(size_t bytes, char *prefix)
{
	if(bytes > 1024)
	{
		if(bytes > 1048576)
		{
			*prefix = 'M';
			return bytes / 1048576 + 1;
		}

		*prefix = 'K';
		return bytes / 1024 + 1;
	}
	
	*prefix = ' ';
	return bytes;
}

int main(int argc, char **argv)
{
	int                 true = TRUE;
	int                 lport, rport;
	char               *lhost = NULL,
	                   *rhost = NULL;
	struct sockaddr_in  laddr, raddr;
	size_t              i, client = 0;

	if(argc != 3)
	{
usage:
		fprintf(stderr, USAGE_STRING);
		return 1;
	}

	signal(SIGINT,  handle_signal);
#ifndef WIN32
	signal(SIGTERM, handle_signal);
	signal(SIGHUP,  handle_signal);
#else
	WSADATA wsaData;
	WORD    wVersionRequested;

	wVersionRequested = MAKEWORD(2, 0);
	if(WSAStartup(wVersionRequested, &wsaData) != 0)
	{
		fprintf(stderr, "winsock: no working winsock.dll was found\n");
		return 1;
	}
#endif

	if(parse_host(argv[1], &lhost, &lport) || parse_host(argv[2], &rhost, &rport))
	{
		if(lhost) free(lhost);
		if(rhost) free(rhost);
		goto usage;
	}

	if(resolve(lhost, &laddr) || resolve(rhost, &raddr))
	{
		free(lhost);
		free(rhost);
		return 1;
	}

	free(lhost);
	free(rhost);

	laddr.sin_family = PF_INET;
	laddr.sin_port = htons(lport);
	bzero(laddr.sin_zero, sizeof(laddr.sin_zero));
	raddr.sin_family = PF_INET;
	raddr.sin_port = htons(rport);
	bzero(raddr.sin_zero, sizeof(raddr.sin_zero));

	printf("tewneln v%s\n%s\n\n", VERSION, ABOUT_STRING);

	if((sock = socket(PF_INET, SOCK_STREAM, 0)) < 0)
	{
		perror("socket");
		goto cleanup;
	}

	if((setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &true, sizeof(true))) < 0)
	{
		perror("setsockopt");
		goto cleanup;
	}

	if((bind(sock, (struct sockaddr *)&laddr, sizeof(struct sockaddr_in))) < 0)
	{
		perror("bind");
		goto cleanup;
	}

	if(listen(sock, 8) < 0)
	{
		perror("listen");
		goto cleanup;
	}

	bzero(clients, sizeof(struct client *) * MAX_CLIENTS);

	for(;;)
	{
		int lsock, rsock;

		if((lsock = accept(sock, NULL, NULL)) < 0)
		{
			perror("accept");
			continue;
		}

		if(client == MAX_CLIENTS)
		{
			for(i = 0; i < MAX_CLIENTS; i++)
				if(client_clean(clients[i]))
					clients[i] = NULL;

			for(i = 0; i < MAX_CLIENTS; i++)
				if(clients[i] == NULL)
				{
					client = i;
					break;
				}

			if(client == MAX_CLIENTS)
			{
				puts("maximum clients reached");
				close(lsock);
				continue;
			}
		}

		if((rsock = socket(PF_INET, SOCK_STREAM, 0)) < 0)
		{
			close(lsock);
			perror("socket");
			continue;
		}

		if(connect(rsock, (struct sockaddr *)&raddr, sizeof(raddr)) < 0)
		{
			close(lsock);
			perror("connect");
			continue;
		}

		if((clients[client++] = client_new(lsock, rsock)) == NULL)
		{
			close(lsock);
			close(rsock);
			continue;
		}

		puts("client connected");
	}

	close(sock);

cleanup:
	finish();
	puts("clean exit");

	return 0;
}

