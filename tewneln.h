#ifndef __TEWNELN_H
#define __TEWNELN_H

#define VERSION "0.2.2"
#define ABOUT_STRING "coded by snow"
#define USAGE_STRING "usage: tewneln local_addr[:local_port] remote_hostname[:remote_port]\n"

#ifndef TRUE
#define TRUE (1)
#endif
#ifndef FALSE
#define FALSE (0)
#endif

#define MAX(a, b) (a > b ? a : b)
#define MIN(a, b) (a < b ? a : b)

#define BUFFER_SIZE 1024
#define MAX_CLIENTS 10

#endif

