#ifndef __CLIENT_H
#define __CLIENT_H

struct client
{
	int             lsock, rsock;
	size_t          dl, ul;
	int             quit : 1;

	pthread_t       thread;
	pthread_mutex_t mutex;
};

struct client *client_new(int, int);
void client_destroy(struct client *);

#endif
